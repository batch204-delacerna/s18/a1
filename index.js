console.log("Hello World!");

// Create functions which are able to display and return results of mathematical operations.
// Create functions which are able to return the area of a circle, the average of four numbers and check if a scoring percentage is passed or failed.

// 1. Function to add two numbers.

function addNumbers(number1, number2) 
{
	console.log("Displayed sum of " + number1 + " and " + number2);
	console.log(number1 + number2);
}

addNumbers(5,15);

// 2. Function to subtract two numbers.

function subtractNumbers(number1, number2) 
{
	console.log("Displayed difference of " + number1 + " and " + number2);
	console.log(number1 - number2);
}

subtractNumbers(20,5);


// 3. Function to multiply two numbers.

function multiplyNumbers(number1, number2) 
{
	return	"The product of " + number1 + " and " + number2 + ":\n" + number1*number2;
}

let product = multiplyNumbers(50,10);
console.log(product);

// 4. Function to divide two numbers.

function divideNumbers(number1, number2) 
{
	return	"The quotient of " + number1 + " and " + number2 + ":\n" + number1/number2;
}

let quotient = divideNumbers(50,10);
console.log(quotient);


// 11. Function to get total area of a circle from a provided raidus. 

function areaOfCircle(constant)
{
	return "The result of getting the area of a circle with " + constant + " radius:\n" + Math.round((Math.PI * constant**2 + Number.EPSILON) * 100) / 100;
}

// 12
let circleArea = areaOfCircle(15);
console.log(circleArea);

// 13. function to get total average of four numbers.

function averageOfFourNumbers(number1, number2, number3, number4)
{
	// Think of a way to accommodate infinitely many parameters in this array. If this function is updated to 5, 6 or N number of parameters, the array should accommodate it. Somewhat dynamic. 
	
	let arrayNumbers = [number1, number2, number3, number4]
	function add(accumulator, a)
	{
		return (accumulator + a)
	}
	const averageOfArray = arrayNumbers.reduce(add,0)/arrayNumbers.length;

	return "The average of " + number1 + "," + number2 + "," + number3 + " and " +number4 + ":\n" + averageOfArray;
}


// 14
let averageVar = averageOfFourNumbers(20, 40, 60, 80);
console.log(averageVar);

// let arrayNumbers = [1,2,3,4,5];
// const averageOfArray = arrayNumbers.reduce(add,0)/arrayNumbers.length;
// function add(accumulator, a)
// {
// 	return (accumulator + a)
// }
// console.log(averageOfArray);

// 15. Function to check if you passed by checking the percentage of your scroe against the passing percentage.

function isPassedOrFailed(score1, score2)
{
	let percentage = (score1 / score2) * 100;
	let isPassed = percentage >= 75;

	return "Is " + score1 + "/" + score2 + " a passing score?\n" + isPassed;
}

let isPassingScore = isPassedOrFailed(38,50);
console.log(isPassingScore);



